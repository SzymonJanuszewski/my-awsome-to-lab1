package com.company;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {

        XMLDownloader downloader = new XMLDownloader();
        downloader.setUrl("https://www.nbp.pl/kursy/xml/lasta.xml");
        downloader.getXML();

        ProvideData provideData = new ProvideData();
        CurrencyCollection currencyCollection = provideData.readData(downloader.getFile());

        System.out.println("dostępne waluty: \n");
        currencyCollection.showCurrencies();

        System.out.print("podaj kod swojej waluty: ");
        Converter converter = new Converter();

        try {
            Scanner scanner = new Scanner(System.in);
            String code1 = scanner.nextLine();
            converter.setCode1(code1);
            if (currencyCollection.getCurrencyByCode(code1) == null) {
                System.out.println("nie ma podanej waluty!");
                System.exit(1);
            }
        } catch (Exception e) {
            System.out.println(" niepoprawny kod ");
        }
        System.out.print("podaj kwote: ");
        try {
            Scanner inputValue = new Scanner(System.in);
            float value = inputValue.nextFloat();
            converter.setAmount(value);
            if (value < 0) {
                System.out.println("wartosc musi być >= 0");
                System.exit(1);
            }

        } catch (Exception e) {
            System.out.println("wymagana jest liczba");
            System.exit(1);
        }

        System.out.print("kod waluty na ktora chesz wymienic");
        try {
            Scanner scanner = new Scanner(System.in);
            String code2 = scanner.nextLine();
            converter.setCode2(code2);
            if (currencyCollection.getCurrencyByCode(code2) == null) {
                System.out.println("nie ma podanej waluty");
                System.exit(1);
            }
        } catch (Exception e) {
            System.out.println("niepoprawne dane");
            System.exit(1);
        }
        System.out.println(": " + converter.exchange(currencyCollection) + " " + converter.getCode2());
        System.exit(0);
    }
}
