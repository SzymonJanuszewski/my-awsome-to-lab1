package com.company;

public class Converter {
    private float amount;
    private String code1;
    private String code2;

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void setCode1(String code1) {
        this.code1 = code1;
    }

    public String getCode2() {
        return code2;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }

    public float exchange(CurrencyCollection currencyCollection) {
        float rate1 = currencyCollection.getCurrencyByCode(code1).getRate();
        float rate2 = currencyCollection.getCurrencyByCode(code2).getRate();
        int factor1 = currencyCollection.getCurrencyByCode(code1).getFactor();
        int factor2 = currencyCollection.getCurrencyByCode(code2).getFactor();

        return(amount*rate1*factor1*factor2)/rate2;
    }
}
