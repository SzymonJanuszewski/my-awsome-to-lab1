package com.company;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;

public class ProvideData {
    public CurrencyCollection readData(String data) throws IOException {
        CurrencyCollection currencyList = new CurrencyCollection();
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(data);
            NodeList nodeList = document.getElementsByTagName("pozycja");
            ArrayList<Currency> tempArray = new ArrayList<>();
            for (int i=0;i<nodeList.getLength();i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    Currency currency = new Currency();
                    String currencyName = element.getElementsByTagName("nazwa_waluty").item(0).getTextContent();
                    currency.setName(currencyName);
                    String currencyCode = element.getElementsByTagName("kod_waluty").item(0).getTextContent();
                    currency.setCode(currencyCode);
                    String factor = element.getElementsByTagName("przelicznik").item(0).getTextContent();
                    currency.setFactor(Integer.parseInt(element.getElementsByTagName("przelicznik").item(0).getTextContent()));
                    float avgRate = Float.parseFloat(element.getElementsByTagName("kurs_sredni").item(0).getTextContent().replace(",","."));
                    currency.setRate(avgRate);
                    tempArray.add(currency);
                }
            }
            currencyList.setCurrencyCollectionList(tempArray);

        } catch (ParserConfigurationException | SAXException e) {
            e.printStackTrace();

        }
        return currencyList;
    }
}
