package com.company;

import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class XMLDownloader {
    public final String file = "data.xml";
    private String url;

    public String getFile() {
        return file;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void getXML() {
        try {
            FileWriter writer = new FileWriter("data.xml");
            URL oracle = new URL(url);
            InputSource source = new InputSource(oracle.openStream());
            source.setEncoding("utf-8");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(source.getByteStream()));
            String inputLine;
            int i = 0;
            while ((inputLine = bufferedReader.readLine()) != null) {
                writer.write(inputLine + '\n');
                if (i == 2) {
                    writer.write("<pozycja>\n<nazwa_waluty>polski zloty</nazwa_waluty>\n<przelicznik>1</przelicznik>\n<kod_waluty>PLN</kod_waluty>\n<kurs_sredni>1</kurs_sredni>\n</pozycja>");
                }
                i++;
            }
            bufferedReader.close();
            writer.close();
            System.out.println("Plik zapisany");
        } catch (IOException e) {
            System.out.println("Blad zapisu");
        }
    }


}
