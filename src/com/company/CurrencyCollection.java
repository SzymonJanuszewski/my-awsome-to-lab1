package com.company;

import java.util.ArrayList;

public class CurrencyCollection implements ICurrencyCollection {
    private ArrayList<Currency> currencyCollectionList = new ArrayList<>();

    @Override
    public ArrayList<Currency> getCurrencyCollectionList() {
        return currencyCollectionList;
    }

    public void setCurrencyCollectionList(ArrayList<Currency> newList) {
        this.currencyCollectionList = newList;
    }

    public Currency getCurrencyByCode(String code) {
        return currencyCollectionList
                .stream().filter(Currency -> code.
                        equals(Currency.getCode()))
                .findFirst()
                .orElse(null);
    }

    public void showCurrencies() {
        for (Currency item : currencyCollectionList) {
            System.out.println("Nazwa: " + item.getName() + " Kod: " + item.getCode() + " Kurs: " + item.getRate() + " Przelicznik: " + item.getFactor());
        }
    }
}
