package com.company;

public interface ICurrency {
    String getName();
    String getCode();
}
