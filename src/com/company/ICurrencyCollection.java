package com.company;

import java.util.ArrayList;

public interface ICurrencyCollection {
    ArrayList<Currency> getCurrencyCollectionList();

    Currency getCurrencyByCode(String code);
}
